﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace DebuggingTask
{
    class Program
    {
        static void Main(string[] args)
        {
            NetworkInterface networkInterface = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault();
            var addressBytes = networkInterface.GetPhysicalAddress().GetAddressBytes();
            var dateBytes = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());

            var arrOfInts = addressBytes.Select((x, i) => { return (x ^ dateBytes[i]) * 10; });
            var key = new StringBuilder();
            foreach (var _int in arrOfInts)
            {
                key.Append(_int);
                key.Append("-");
            }
            key.Remove(key.Length - 1, 1);
            Console.WriteLine(key);
            Console.ReadLine();
        }
    }
}
